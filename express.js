const express = require('express')
const bodyParser = require('body-parser');
const app = express()
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
let users = [
    {
      "_id": "1",
      "name": "Ryu",
      "health": 45, 
      "attack": 4, 
      "defense": 3,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
    },
    {
      "_id": "2",
      "name": "Dhalsim",
      "health": 60, 
      "attack": 3, 
      "defense": 1,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif"
    },
    {
      "_id": "3",
      "name": "Guile",
      "health": 45, 
      "attack": 4, 
      "defense": 3,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/guile-hdstance.gif"
    },
    {
      "_id": "4",
      "name": "Zangief",
      "health": 60, 
      "attack": 4, 
      "defense": 1,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/zangief-hdstance.gif"
    },
    {
      "_id": "5",
      "name": "Ken",
      "health": 45, 
      "attack": 3, 
      "defense": 4,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ken-hdstance.gif"
    },
    {
      "_id": "6",
      "name": "Bison",
      "health": 45, 
      "attack": 5, 
      "defense": 4,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif"
    },
    {
      "_id": "7",
      "name": "Chun-Li",
      "health": 40, 
      "attack": 3, 
      "defense": 8,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/chunli-hdstance.gif"
    },
    {
      "_id": "8",
      "name": "Blanka",
      "health": 80, 
      "attack": 8, 
      "defense": 2,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/blanka-hdstance.gif"
    },
    {
      "_id": "9",
      "name": "E.Honda",
      "health": 50, 
      "attack": 5, 
      "defense": 5,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ehonda-hdstance.gif"
    },
    {
      "_id": "10",
      "name": "Balrog",
      "health": 55, 
      "attack": 4, 
      "defense": 6,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/balrog-hdstance.gif"
    },
    {
      "_id": "11",
      "name": "Vega",
      "health": 50, 
      "attack": 4, 
      "defense": 7,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/vega-hdstance.gif"
    },
    {
      "_id": "12",
      "name": "Sagat",
      "health": 55, 
      "attack": 4, 
      "defense": 6,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/sagat-hdstance.gif"
    },
    {
      "_id": "13",
      "name": "Cammy",
      "health": 45, 
      "attack": 4, 
      "defense": 7,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/cammy-hdstance.gif"
    },
    {
      "_id": "14",
      "name": "Fei Long",
      "health": 80, 
      "attack": 2, 
      "defense": 3,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/feilong-hdstance.gif"
    },
    {
      "_id": "15",
      "name": "Dee Jay",
      "health": 50, 
      "attack": 5, 
      "defense": 5,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/deejay-hdstance.gif"
    },
    {
      "_id": "16",
      "name": "T.Hawk",
      "health": 60, 
      "attack": 5, 
      "defense": 3,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/thawk-hdstance.gif"
    },
    {
      "_id": "17",
      "name": "Akuma",
      "health": 70, 
      "attack": 5, 
      "defense": 5,
      "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/akuma-hdstance.gif"
    }
  ]

app.get('/user', function (req, res) {
    res.json(users)
})

app.get('/user/:id', function (req, res) {
    users.forEach((user) => {
        if (user["_id"] == req.params.id) {
            res.json(user)
        }
    })
})

app.post('/user', function (req, res) {
    users.push(req.body)
    res.send(req.body)
  })

app.delete('/user/:id', function (req, res) {
    let user = users[req.params.id - 1]
    users.splice(req.params.id - 1, 1)
    res.send(user)
  })

  app.put('/user/:id', function (req, res) {
    users = users.map((user) => {
        if (user["_id"] == req.params.id) {
            user = req.body
        }
        return user
    })
    res.send(req.body)
})


app.get('/:param', function (req, res) {
    console.log(req.query)
    console.log(req.params)
  res.send('Hello World')
})

app.put('/', function (req, res) {
  res.send('Hello World')
})
 
app.listen(3000)