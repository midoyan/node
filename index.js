const http = require('http');
const port = 3000;
const fs = require('fs');
const logFile = fs.createWriteStream('log.txt', {flags: 'a'});
const requestHandler = (request, response) => {
    console.log(request.url);
    logFile.write(`Запрос по адресу: ${request.url}\r\n`)
    response.end('Ping-Pong');
};
const server = http.createServer(requestHandler);
server.listen(port, (err) => {
    if (err) {
        return console.log('Ошибочка вышла', err);
    }
    console.log(`Сервер запущен по адресу http://localhost:${port}`);
})